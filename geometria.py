def calc_circ_area(r):
    """calcula área de circulo con radio r
        r: valor numérico del radio
        retorna: valor área del círculo
    """
    area = 3.1415 * r * r
    return area

def calc_circ_perimetro(r):
    """calcula perímetro de un círculo de radio r"""
    perim = 2*3.1415*r
    return perim

def volumen_cilindro(r, h):
    """calcula volumen de cilindro con radio r y altura h"""
    area = calc_circ_area(r)
    vol = area * h
    return vol
