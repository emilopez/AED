# Algoritmos y Estructuras de Datos en Python

Las diapositivas utilizadas pueden ser visualizadas o ejecutadas en vivo:

- Para visualizarlas: https://emilopez.gitlab.io/AED/
- Para ejecutar los notebooks jupyter online clickear sobre [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/emilopez%2FAED/HEAD) y luego sobre la clase (.ipynb) que desee.


## Contenidos

- [Clase 13 A:](https://emilopez.gitlab.io/AED/clase13-A.html) variables / tipo de datos / operadores 
- [Clase 13 B:](https://emilopez.gitlab.io/AED/clase13-B.html) if / while / for
- [Clase 15:](https://emilopez.gitlab.io/AED/clase15.html) estructuras de datos arreglos / listas 
